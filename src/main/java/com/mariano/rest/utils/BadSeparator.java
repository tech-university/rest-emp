package com.mariano.rest.utils;

public class BadSeparator extends Exception{
    @Override
    public String getMessage(){
        return "Separador incorrecto";
    }
}
