package com.mariano.rest.utils;

public enum EstadosPedido {
    ACEPTADO,
    COCINANDO,
    EN_ENTREGA,
    ENTREGADO,
    VALORADO;

}
