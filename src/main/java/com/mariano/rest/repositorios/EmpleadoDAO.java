package com.mariano.rest.repositorios;

import com.mariano.rest.empleados.Capacitacion;
import com.mariano.rest.empleados.Empleado;
import com.mariano.rest.empleados.Empleados;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


@Repository
public class EmpleadoDAO {
    Logger logger = LoggerFactory.getLogger(EmpleadoDAO.class);

    private static Empleados list = new Empleados();
    static {

        Capacitacion cap1 = new Capacitacion("2020/01/02","DBA");
        Capacitacion cap2 = new Capacitacion("2019/01/02","Front End");
        Capacitacion cap3 = new Capacitacion("2018/01/02","BackEnd");
        ArrayList<Capacitacion> una = new ArrayList<Capacitacion>();
        una.add(cap1);
        ArrayList<Capacitacion> dos = new ArrayList<Capacitacion>();
        dos.add(cap1);
        dos.add(cap2);
        ArrayList<Capacitacion> todas = new ArrayList<Capacitacion>();
        todas.add(cap3);
        todas.addAll(dos);

        list.getListaEmpleados().add(new Empleado(1,"Mariano","Torres", "mariano.torres@bbva.com",una));
        list.getListaEmpleados().add(new Empleado(2,"Sonia","Arroyo", "soni123@correo.com",dos));
        list.getListaEmpleados().add(new Empleado(3,"Andres","Mendoza", "andres345@correo.com",todas));



    }
    public Empleados getAllEmpleados(){
        logger.debug("Empleados devueltos");
        return list;
    }

    public Empleado getEmpleado(int id){
        for (Empleado emp : list.getListaEmpleados()){
            if (emp.getId() == id){
                return emp;

            }

        }
        return null;
    }
    public void addEmpleado(Empleado emp){
        list.getListaEmpleados().add(emp);
    }

    public void updEmpleado(Empleado emp){
        Empleado    current = getEmpleado(emp.getId());
                    current.setId(emp.getId());
                    current.setNombre(emp.getNombre());
                    current.setApellido(emp.getApellido());
                    current.setEmail(emp.getEmail());
    }
    public void updEmpleado(int id, Empleado emp){
        Empleado    current = getEmpleado(emp.getId());
        current.setId(emp.getId());
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());
        current.setEmail(emp.getEmail());
    }
    /*
    public String deleteEmpleado(int id){
        Empleado current = getEmpleado(id);
        if (current == null) return "NO";

        Iterator it =list.getListaEmpleados().iterator();
        while (it.hasNext()){
            Empleado emp = (Empleado) it.next();
            if (emp.getId() == id) {
                it.remove();
                break;
            }
        }
        return "OK";
    }

     */

    //Otra forma de hacer el dalete por java 8
    public String deleteEmpleado(int id){
        if(list.getListaEmpleados().removeIf(empleado -> empleado.getId()==id)){
            return "ok";
        }else{
            return "nok";
        }
    }

    public void softupdEmpleado(int id, Map<String,Object> updates){
        Empleado current = getEmpleado(id);
        for (Map.Entry<String,Object> update : updates.entrySet()){
            switch (update.getKey()){
                case "nombre":
                    current.setNombre(update.getValue().toString());
                    break;
                case "apellido":
                    current.setApellido(update.getValue().toString());
                case "email":
                    current.setEmail(update.getValue().toString());
            }
        }
    }
    public List<Capacitacion> getCapacitacionesEmpleado(int id){
        Empleado current = getEmpleado(id);
        ArrayList<Capacitacion> caps = new ArrayList<Capacitacion>();
        if (current != null) caps = current.getCapacitaciones();
        return  caps;
    }
    public Boolean addCapacitacion(int id, Capacitacion cap){
        Empleado current = getEmpleado(id);
        if(current == null) return false;
        current.getCapacitaciones().add(cap);
        return true;
    }
}
