package com.mariano.rest;

import com.mariano.rest.utils.BadSeparator;
import com.mariano.rest.utils.EstadosPedido;
import com.mariano.rest.utils.Utilidades;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;

@EnableConfigurationProperties
@SpringBootTest
public class EmpleadosControllerTest {
    @Autowired
    EmpleadosController empleadosController;
    @Test
    public void testGetCadena(){
        String correcto = "L.U.Z D.E.L S.O.L";
        String origen = "luz del sol";
        Assertions.assertEquals(correcto,empleadosController.getCadena(origen,"."));
    }

    @Test
    public void testSeparadorGetCadena(){
        try{
            Utilidades.getCadena("\"Mariano Torres\""," ");
            fail("Se esperaba BadSeparator");
        }catch (BadSeparator bs){}
    }
    @Test
    public void testGetAutor(){
        Assertions.assertEquals("\"Mariano Torres\"", empleadosController.configuracion.getAutor());
    }

    @ParameterizedTest
    @ValueSource(strings = {""," "})
    public void testEstaBlanco(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ","\t","\n"})
    public void testEstaBlancoCompleto(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 3, 5, 17, -3, Integer.MAX_VALUE})
    public void testEsImpar(int numero){
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testValorarEstadospedido(EstadosPedido ep){
        assertTrue(Utilidades.valorarEstadoPedido(ep));
    }
}
